# Stukkenparser-cli

NB: de actieve versie van dit project staat hier: https://gitlab.com/minfin-bbh/stukkenparser-cli

Commandline interface voor de stukkenparser. Installeer met Pip:

~~~
$ pip install git+https://gitlab.com/proprefenetre/stukkenparser-cli@main
~~~

## Usage

~~~
Usage: sparser [OPTIONS]

Options:
  -a, --add PATH
  -u, --update TEXT
  -c, --cfg PATH
  --help             Show this message and exit.
  ~~~

Met `-a` voeg je een document of een map met documenten toe. Met `-u` worden alle bestanden onder `path` geupdate,
e.g. `sparser -u 2019/JV` update alle bestanden van jaarverslag 2019.

Geparste documenten worden standaard opgeslagen in:

- Windows: C:/Users/<USER>/AppData/Roaming/stukkenparser
- Linux: /home/<USER>/.local/share/stukkenparser
