from datetime import datetime
import json
import os
from pathlib import Path
import shutil

# import sys
from typing import Any, List, Dict, Optional, Union

from appdirs import user_data_dir
import click
from stukkenparser import Parser, XMLAdapter, Stuk
from tqdm import tqdm


DEFAULT_CFG = Path(__file__).parent.resolve() / "conf/default_config.json"
DATA_DIR = Path(user_data_dir()) / "stukkenparser"
DATA_DIR.mkdir(exist_ok=True, parents=True)


def path_cb(ctx, param, fname):
    if fname:
        return Path(fname)


def attrs(ond):
    "Add default attributes to xml elements."
    return {
        attr: str(getattr(ond, attr, None))
        for attr in ["id", "index", "level"]
        if hasattr(ond, attr)
    }


def log(stuk: Stuk, fp: Path) -> None:
    """Log the state of a parsed document."""
    empties = stuk.find(lambda o: hasattr(o, "children") and len(o.children) == 0)

    log = {
        "parsed_on": str(datetime.now()),
        "chapter": stuk.chapter,
        "year": stuk.year,
        "phase": stuk.phase,
        "dossier": stuk.dossier,
        "source": stuk.metadata.get("source"),
        "empty_sections_num": len(empties),
        "empty_sections": [[e.id, e.text] for e in empties],
        "config": stuk.config,
    }

    with open(fp, "w", encoding="utf-8") as f:
        json.dump(log, f, indent=4)


def write_xml(stuk: Stuk, fp: Path, cbs: Dict[str, Any]) -> None:
    adp = XMLAdapter(stuk, callbacks=cbs)
    with open(fp, "w", encoding="utf-8") as out:
        out.write(adp.to_string())


def add_doc(fp: Path, cfg: Dict[str, Any]) -> None:
    """Add a new html-file to the warehouse. """
    parser = Parser(cfg)
    stuk = parser.parse(fp, from_file=True)
    dest = Path(f"{DATA_DIR.resolve()}/{stuk.year}/{stuk.phase}/{stuk.chapter}")

    if not dest.exists():
        try:
            dest.mkdir(parents=True)
        except PermissionError as e:
            raise PermissionError(
                f"Permission denied: '{dest}' (data dir: '{DATA_DIR}')"
            ) from e

    shutil.copyfile(fp, dest / fp.name)

    with open(
        dest / fp.with_suffix(".xml").name,
        "w",
        encoding="utf-8",
    ) as out:
        adp = XMLAdapter(stuk, callbacks={"attrs": attrs})
        out.write(adp.to_string())

    log(stuk, dest / f"{fp.stem}-log.json")


def update_docs(path: str, glob_cfg: Dict[str, Any]) -> None:
    parser = Parser(glob_cfg)

    data = DATA_DIR / path
    if not data.exists():
        raise FileNotFoundError(f"Not in warehouse: '{path}'")

    # for fp in data.glob("**/*.html"):
    with tqdm(list(data.glob("**/*.html"))) as queue:
        for fp in queue:
            queue.set_description(f"{fp.name}")
            dest = fp.parent
            if (dest / "config.json").exists():
                with (dest / "config.json").open("r", encoding="utf-8") as cfg_f:
                    loc_cfg = json.load(cfg_f)
            else:
                loc_cfg = {}
                with (dest / "config.json").open("w", encoding="utf-8") as cfg_f:
                    json.dump(loc_cfg, cfg_f)

            stuk = parser.parse(fp, loc_cfg, from_file=True)

            write_xml(stuk, dest / fp.with_suffix(".xml"), {"attrs": attrs})
            log(stuk, dest / f"{fp.stem}-log.json")


def bootstrap() -> None:
    if not DATA_DIR.exists():
        DATA_DIR.mkdir(parents=True)

    if not (DATA_DIR / "config.json").exists():
        shutil.copyfile(DEFAULT_CFG, DATA_DIR / "config.json")


@click.command()
@click.option(
    "-a", "--add", type=click.Path(exists=True, readable=True), callback=path_cb
)
@click.option("-u", "--update", type=str)
@click.option(
    "-c", "--cfg", type=click.Path(exists=True, readable=True), callback=path_cb
)
def run(add: Path, update: str, cfg: Path):

    bootstrap()

    cfg_loc = DATA_DIR / "config.json"

    with cfg_loc.open("r") as f:
        config = json.load(f)

    if add:
        if add.is_dir():
            with tqdm(list(add.iterdir())) as queue:
                for f in queue:
                    queue.set_description(f"{f.name}")
                    try:
                        add_doc(f, config)
                    except Exception as e:
                        # error_doc = f
                        print(f"Error: {f}: {e}")
                # add_doc(error_doc)
        else:
            add_doc(add, config)

    if update:
        update_docs(update, config)


if __name__ == "__main__":
    run()
