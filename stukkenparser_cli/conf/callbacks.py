from stukkenparser import Kop
import re
import roman


def chapter_to_dec(chapter):
    if chapter in ("A", "B", "C", "J"):
        return chapter
    if any(chapter.endswith(x) for x in ["A", "B"]):
        *chapter, suff = list(chapter)
        return str(roman.fromRoman("".join(chapter))) + suff
    return str(roman.fromRoman(chapter))


def ftext(ond):
    if isinstance(ond, Kop):
        if re.search(r"beleidsartikelen", ond.parent.tag, re.I):
            m = re.match(
                r"(\d(\.\d)+ )?((niet-)?beleids)?artikel (\d+)\.?", ond.text, re.I
            )
            if m:
                return {
                    "ftext.ReferencesBudgetStructureYear": f"{ond.year}",
                    "ftext.ReferencesBudgetChapterNumber": f"{chapter_to_dec(ond.chapter)}",
                    "ftext.ReferencesBudgetArticleNumber": f"{m.groups()[-1]}",
                }


def default_attrs(ond):
    return {
        attr: str(getattr(ond, attr, None))
        for attr in ["id", "index"]
        if hasattr(ond, attr)
    }
